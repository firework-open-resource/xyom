<%--
  Created by IntelliJ IDEA.
  User: wangshibo
  Date: 2020/2/29
  Time: 21:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="author" content="order by dede58.com"/>
    <title>用户注册</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/login.css">
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/js1.js"></script>

</head>
<body>
<form  method="post" action="<%=request.getContextPath()%>/userServlet" id="submitInfo">
    <div class="regist">
        <div class="regist_center">
            <div class="regist_top">
                <div class="left fl">会员注册</div>
                <div class="right fr"><a href="<%=request.getContextPath()%>/index2.jsp" target="_self">xueyuan商城</a></div>
                <div class="clear"></div>
                <div class="xian center"></div>
            </div>
            <div class="regist_main center">
                <input type="hidden" name="method" value="regedit">
                <div class="user1">用&nbsp;&nbsp;户&nbsp;&nbsp;名:&nbsp;&nbsp;<input class="shurukuang" type="text" name="username" id="username" placeholder="用户名"/><span id="s_username"></span></div>
                <div class="user1">密　　码:&nbsp;&nbsp;<input class="shurukuang" type="password"  name="password" id="password" placeholder="密码"/><span id="s_password"></span></div>

                <div class="user1">确认密码:&nbsp;&nbsp;<input class="shurukuang" type="password" name="repassword" id="repassword"  placeholder="确认你的密码"/><span id="s_repassword"></span></div>
                <div class="user1">邮　　箱:&nbsp;&nbsp;<input class="shurukuang" type="text" name="email" id="email"  placeholder="邮箱"/><span id="s_email"></span></div>
                <div class="user1">姓　　名:&nbsp;&nbsp;<input class="shurukuang" type="text" name="name" id="name"  placeholder="姓名"/><span id="s_name"></span></div>
                <div class="user1">性　　别:&nbsp;&nbsp;<span class="shurukuang" style="border: none; color: black"><input  type="radio"  name="sex" value="男" checked>男<input  type="radio" style="margin-left: 7px" name="sex" value="女">女</span><span></span></div>
                <div class="user1">出生日期:&nbsp;&nbsp;<input type="date" name="birthday"><span></span></div>
                <%--<div class="user1">--%>
                    <%--<div class="left fl">验&nbsp;&nbsp;证&nbsp;&nbsp;码:&nbsp;&nbsp;<input class="yanzhengma" type="text" name="user_yanz" placeholder="验证码"/></div>--%>
                    <%--<div class="right fl"><img src="/image/yanzhengma.jpg"></div>--%>
                    <%--<div class="clear"></div>--%>
                <%--</div>--%>
            </div>
            <div class="regist_submit" >
                <button class="submit"  name="submit"  style="margin-bottom: 30px"  onclick="return check()">立即注册</button>
            </div>


        </div>
    </div>
</form>
</body>
</html>

