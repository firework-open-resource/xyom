<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: wangshibo
  Date: 2020/4/19
  Time: 21:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style.css">
    <script src="http://libs.baidu.com/jquery/2.1.4/jquery.min.js"></script>

</head>
<body>
<%@include file="head.jsp"%>
<div class="xiantiao"></div>
<div class="gwcxqbj" style="height: auto;">

    <div class="gwcxd center">

        <div class="top2 center">
            &nbsp;订单编号：${bean.oid}
        </div>
            <%--标题栏--%>
            <div class="top2 center" style="height: 44px;height: 44px;">
                <div class="sub_top fl" style="width: 50px;height: 44px;"></div>
                <div class="sub_top fl" style="margin-left: 50px;height: 44px;">商品名称</div>
                <div class="sub_top fl" style="height: 44px">单价</div>
                <div class="sub_top fl"  style="height: 44px">数量</div>
                <div class="sub_top fl" style="width: 200px;height: 44px;">小计</div>
                <div class="sub_top fl"  style="height: 44px"></div>


                <div class="clear"></div>
            </div>

            <c:forEach items="${bean.items}" var="ci">

                <div class="content2 center" style="height: 100px">
                    <div class="sub_top fl" style="height: 100px"></div>
                    <div class="sub_content fl" style="height: 100px"><img src="<%=request.getContextPath()%>${ci.product.pimage}" style="margin: 3% auto;"></div>
                    <div class="sub_content fl ft20" style="margin-top:-1%;height: 100px">${ci.product.pname}</div>
                    <div class="sub_content fl " style="margin-top:-1%;height: 100px">${ci.product.shop_price}</div>
                    <div class="sub_content fl" style="margin-top:-1%;height: 100px">
                        <input class="shuliang" readonly type="number" value="${ci.count}" step="1" min="1" >
                    </div>
                    <div class="sub_content fl" style="margin-top:-1%;height: 100px">${ci.subtotal}</div>
                    <div class="sub_top fl"></div>
                    <div class="clear"></div>
                </div>

            </c:forEach>
    </div>

    <div class="jiesuandan mt20 center" style="margin-top: 2px">

        <div class="jiesuan fr">
            <div class="jiesuanjiage fl">合计（不含运费）：<span>${bean.total}元</span></div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>

    <div class="jiesuandan mt20 center" style="display: grid;height: 200px;">

        <div class="jiesuan fr">
            &nbsp;　　收&nbsp;&nbsp;件&nbsp;&nbsp;人：<input type="text">
        </div>
        <div class="jiesuan fr">
            &nbsp;　　联系电话：<input type="text">
        </div>
        <div class="jiesuan fr">
            &nbsp;　　收件地址：<input type="text">
        </div>
        <div class="clear"></div>
    </div>

</div>


<footer class="center">

    <div class="mt20">小米商城|MIUI|米聊|多看书城|小米路由器|视频电话|小米天猫店|小米淘宝直营店|小米网盟|小米移动|隐私政策|Select Region</div>
    <div>©mi.com 京ICP证110507号 京ICP备10046444号 京公网安备11010802020134号 京网文[2014]0059-0009号</div>
    <div>违法和不良信息举报电话：185-0130-1238，本网站所列数据，除特殊说明，所有数据均出自我司实验室测试</div>
</footer>
</body>
</html>
