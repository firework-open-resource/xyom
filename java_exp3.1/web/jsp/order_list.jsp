<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: wangshibo
  Date: 2020/4/20
  Time: 13:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="author" content="order by dede58.com"/>
    <title>我的订单-xueyuan商城</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style.css">
    <script src="http://libs.baidu.com/jquery/2.1.4/jquery.min.js"></script>
</head>
<body>
<!-- start header -->
<%@include file="head.jsp"%>
<!--end header -->
<!-- start banner_x -->
<c:forEach items="${pb.data}" var="o">

<div class="gwcxqbj" style="height: auto;">
    <div class="gwcxd center">
        <%--宽度1226px--%>
        <div class="top2 center" style="width: 1226px;">

            <div class="sub_top fl" style="width:250px;height: 40px; "> &nbsp;订单编号：${o.oid}</div>
            <div class="sub_top fl" style="width:100px;height: 40px; ">
                <c:if test="${o.state==0}"><a href="<%=request.getContextPath()%>/OrderServlet?method=getOrderById&oid=${o.oid}">去付款</a></c:if>
                <c:if test="${o.state==1}">已付款</c:if>
                <c:if test="${o.state==2}">确认收货</c:if>
                <c:if test="${o.state==3}">已完成</c:if>
            </div>
            <div class="sub_top fl" style="width:216px;height: 40px; margin-left: 200px;"> ${o.ordertime}</div>
        </div>
        <%--标题栏--%>
        <div class="top2 center" style="height: 44px;height: 44px;">
            <div class="sub_top fl" style="width: 50px;height: 44px;"></div>
            <div class="sub_top fl" style="margin-left: 50px;height: 44px;">商品名称</div>
            <div class="sub_top fl" style="height: 44px">单价</div>
            <div class="sub_top fl"  style="height: 44px">数量</div>
            <div class="sub_top fl" style="width: 200px;height: 44px;">小计</div>
            <div class="sub_top fl"  style="height: 44px"></div>


            <div class="clear"></div>
        </div>

        <c:forEach items="${o.items}" var="oi">

            <div class="content2 center" style="height: 100px">
                <div class="sub_top fl" style="height: 100px"></div>
                <div class="sub_content fl" style="height: 100px"><img src="<%=request.getContextPath()%>${oi.product.pimage}" style="margin: 3% auto;"></div>
                <div class="sub_content fl ft20" style="margin-top:-1%;height: 100px">${oi.product.pname}</div>
                <div class="sub_content fl " style="margin-top:-1%;height: 100px">${oi.product.shop_price}</div>
                <div class="sub_content fl" style="margin-top:-1%;height: 100px">
                    <input class="shuliang" readonly type="number" value="${oi.count}" step="1" min="1" >
                </div>
                <div class="sub_content fl" style="margin-top:-1%;height: 100px">${oi.subtotal}</div>
                <div class="sub_top fl"></div>
                <div class="clear"></div>
            </div>

        </c:forEach>
    </div>

    <div class="jiesuandan mt20 center" style="margin-top: 2px">

        <div class="jiesuan fr">
            <div class="jiesuanjiage fl">合计（不含运费）：<span>${o.total}元</span></div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>

</div>
</c:forEach>


<%--分页--%>
<div class="main center mb20">
    <p align="center">

        <c:if test="${pb.pageNumber>1}">
            <a href="<%=request.getContextPath()%>/OrderServlet?method=findMyOrderGetByPage&pageNumber=1" >首页</a>
            <a href="<%=request.getContextPath()%>/OrderServlet?method=findMyOrderGetByPage&pageNumber=${pb.pageNumber-1}">上一页</a>
        </c:if>
        <c:if test="${pb.pageNumber<pb.totalPage}">
            <a href="<%=request.getContextPath()%>/OrderServlet?method=findMyOrderGetByPage&pageNumber=${pb.pageNumber+1}">下一页</a>
            <a href="<%=request.getContextPath()%>/OrderServlet?method=findMyOrderGetByPage&pageNumber=${pb.totalPage}">末页</a>
        </c:if>

    </p>
    <form action="<%=request.getContextPath()%>/OrderServlet?method=findMyOrderGetByPage" method="post">
        <h4 align="center" style="display: block">
            共${pb.totalPage}页
            <input type="number" style="border: 1px solid saddlebrown;" value="${pb.pageNumber}" min="1" max="${pb.totalPage}"  name="pageNumber" size="1">页
            <input type="submit" value="到达">
        </h4>
    </form>

</div>


<footer class="mt20 center">
    <div class="mt20">小米商城|MIUI|米聊|多看书城|小米路由器|视频电话|小米天猫店|小米淘宝直营店|小米网盟|小米移动|隐私政策|Select Region</div>
    <div>©mi.com 京ICP证110507号 京ICP备10046444号 京公网安备11010802020134号 京网文[2014]0059-0009号</div>
    <div>违法和不良信息举报电话：185-0130-1238，本网站所列数据，除特殊说明，所有数据均出自我司实验室测试</div>
</footer>
</body>
</html>
