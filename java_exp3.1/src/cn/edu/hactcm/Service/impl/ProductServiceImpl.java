package cn.edu.hactcm.Service.impl;

import cn.edu.hactcm.Dao.ProductDao;
import cn.edu.hactcm.Service.ProductService;
import cn.edu.hactcm.domain.PageBean;
import cn.edu.hactcm.domain.Product;

import java.util.List;

public class ProductServiceImpl implements ProductService {
    private ProductDao productDao;

    public void setProductDao (ProductDao productDao) {
        this.productDao = productDao;
    }

    @Override
    /**
     * 查询最新的产品
     * 解耦合
     */
    public List<Product> findNew() {
        //ProductDao productDao = new ProductDaoImpl();
        //ProductDao productDao = (ProductDao)BeanFactory.getBean("ProductDao");
        

        return productDao.findNew();
    }

    @Override
    /**
     * 查询最热产品
     */
    public List <Product> findHot() {
        //ProductDao productDao = new ProductDaoImpl();
        //ProductDao productDao = (ProductDao)BeanFactory.getBean("ProductDao");
        return productDao.findHot();
    }

    @Override
    /**
     * 单个商品详情
     */
    public Product getById(String pid) {
        //ProductDao productDao = new ProductDaoImpl();
        Product pro = productDao.getById(pid);

        return pro;
    }

    @Override
    /**
     * 查询分类商品
     */
    public PageBean<Product> findByPage(int pageNumber, int pageSize, String cid) {
        //ProductDao productDao = new ProductDaoImpl();

        PageBean <Product> pb = new PageBean <>(pageNumber, pageSize);
        List <Product> data = productDao.findByPage(pb, cid);
        pb.setData(data);

        int totalRecord=productDao.getTotalRecord(cid);
        pb.setTotalRecord(totalRecord);


        return pb;
    }
}
