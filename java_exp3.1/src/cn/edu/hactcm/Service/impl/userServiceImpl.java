package cn.edu.hactcm.Service.impl;

import cn.edu.hactcm.Dao.userDao;
import cn.edu.hactcm.Service.userService;
import cn.edu.hactcm.Util.SendEmailThread;
import cn.edu.hactcm.constant.Constant;
import cn.edu.hactcm.domain.user;

public class userServiceImpl implements userService {
    private userDao userDao;

    public void setUserDao (cn.edu.hactcm.Dao.userDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void regedit(user user1) {
        //调用dao，完成注册
        //userDao userDao = new userDaoImpl();
        userDao.save(user1);
        //发送激活邮件
        String emailMsg="恭喜"+user1.getName()+"：成为我们商城的一员，<a href='http://gio1.top:8080/mi/userServlet?method=active&code="+user1.getCode()+"'>点此激活</a>";
        //mailUtil.sendEmail("用户激活",emailMsg,user1.getEmail(),new String[]{});
        SendEmailThread thread=new SendEmailThread("用户激活",emailMsg,user1.getEmail(),new String[]{});
        thread.start();


    }

    @Override
    /**
     * 用户激活
     */
    public user active(String code) {
        //userDao userDao = new userDaoImpl();

        //通过code获取用户
        user user1 = userDao.getByCode(code);
        if (user1 == null) {
            return null;
        }

        //若获取到了，修改用户
        user1.setState(Constant.USER_IS_ACTICE);
        user1.setCode(null);
        userDao.update(user1);
        return user1;
    }

    @Override
    public user login(String username, String password) {
        //userDao userDao = new userDaoImpl();

        return userDao.getByUsernameAndPwd(username, password);
    }
}
