package cn.edu.hactcm.Service.impl;

import cn.edu.hactcm.Dao.CategoryDao;
import cn.edu.hactcm.Service.CategoryService;
import cn.edu.hactcm.Util.jsonUtil;
import cn.edu.hactcm.domain.Category;

import java.util.List;

public class CategoryServiceImpl implements CategoryService {
    private CategoryDao categoryDao;

    public void setCategoryDao (CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    //查询你所有分类
    public String findAll() {
        //调用dao，查询所有分类

        List<Category> list=categoryDao.findAll();

        //将list转换为json字符串
        if (list != null || list.size() > 0) {
            jsonUtil jsonUtil1 = new jsonUtil();
            return jsonUtil1.listToJson(list);
        }
        return null;
    }
}
