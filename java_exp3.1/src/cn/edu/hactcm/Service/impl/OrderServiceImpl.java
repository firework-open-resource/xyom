package cn.edu.hactcm.Service.impl;

import cn.edu.hactcm.Dao.OrderDao;
import cn.edu.hactcm.Service.OrderService;
import cn.edu.hactcm.domain.Order;
import cn.edu.hactcm.domain.OrderItem;
import cn.edu.hactcm.domain.PageBean;


import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class OrderServiceImpl implements OrderService {
    private OrderDao od;

    public void setOd (OrderDao od) {
        this.od = od;
    }

    @Override
    public void save(Order order) {
        //获取dao
        //OrderDao od= (OrderDao)BeanFactory.getBean("OrderDao");
        //开启事务
        //DataSourceUtils
        //向order表中插入一条
        od.save(order);
        //向orderitem中插入n条
        //int i=1/0;
        for (OrderItem oi : order.getItems()) {
            od.saveItem(oi);
        }
        //事务控制

    }

    @Override
    /**
     * 我的订单
     */
    public PageBean<Order> findMyOrderGetByPage(int pageNumber, int pageSize, String uid) throws InvocationTargetException, IllegalAccessException {
        //创建dao
        //OrderDao od = (OrderDao)BeanFactory.getBean("OrderDao");
        //创建pageBean
        PageBean <Order> pb = new PageBean <>(pageNumber, pageSize);
        //查询总条数，设置总条数
        int totalRecord=od.getTotalRecord(uid);
        pb.setTotalRecord(totalRecord);
        pb.setTotalPage();

        //查询当前页数据，设置当前页数据
        List <Order> data = od.findMyOrdersByPage(pb, uid);
        pb.setData(data);
        return pb;
    }

    @Override
    /**
     * 获取订单通过其id
     */
    public Order getOrderById (String oid) throws Exception {
        //OrderDao od=(OrderDao)BeanFactory.getBean("OrderDao");
        return od.getOrderById(oid);
    }
}
