package cn.edu.hactcm.Service;

import cn.edu.hactcm.domain.PageBean;
import cn.edu.hactcm.domain.Product;

import java.util.List;

public interface ProductService {
    List<Product> findNew();

    List<Product> findHot();

    Product getById(String pid);

    PageBean<Product> findByPage(int pageNumber, int pageSize, String cid);
}
