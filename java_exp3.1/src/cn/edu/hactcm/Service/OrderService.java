package cn.edu.hactcm.Service;

import cn.edu.hactcm.domain.Order;
import cn.edu.hactcm.domain.PageBean;

import java.lang.reflect.InvocationTargetException;

public interface OrderService {
    void save(Order order);

    PageBean<Order> findMyOrderGetByPage(int pageNumber, int pageSize, String uid) throws InvocationTargetException, IllegalAccessException;

    Order getOrderById (String oid) throws Exception;
}
