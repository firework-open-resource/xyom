package cn.edu.hactcm.domain;

import org.apache.commons.collections.map.HashedMap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Cart {
    private Map <String, CartItem> itemMap = new HashMap <String, CartItem>();
    private Double total=0.0;

    public Map <String, CartItem> getItemMap() {
        return itemMap;
    }

    public void setItemMap(Map <String, CartItem> itemMap) {
        this.itemMap = itemMap;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    /**
     * 加入购物车
     */
    public void addToCart(CartItem item) {

        String pid = item.getProduct().getPid();
        //判断购物车中是否有
        if (itemMap.containsKey(pid)) {
            //有 修改数量

            //原来的购物项
            CartItem cartItem = itemMap.get(pid);
            cartItem.setCount(cartItem.getCount()+item.getCount());
        }else {
            itemMap.put(pid,item);

        }
        //修改总金额
        total+=item.getSubtotal();
    }

    /**
     * 加入购物车
     */
    public void removeFromCart(String pid) {

        //从购物车（map）中移除 购物项
        CartItem item = itemMap.remove(pid);
        //修改总金额
        total-=item.getSubtotal();

    }

    /**
     * 加入购物车
     */
    public void ClearCart() {
        //清空map集合
        itemMap.clear();
        //修改总金额
        total=0.0;
    }

    /**
     * 获取所有的购物项（就是map集合中所有的value值）
     */
    public Collection<CartItem> getCartItems(){
        return itemMap.values();
    }
}
