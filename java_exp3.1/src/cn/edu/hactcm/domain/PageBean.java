package cn.edu.hactcm.domain;

import java.util.List;

public class PageBean<T> {
    private List<T> data; //当前页的数据
    private int pageNumber;//当前页
    private int totalRecord;//总条数
    private int pageSize;//每页显示数量
    private int totalPage;//总页数

    public PageBean(int pageNumber, int pageSize) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }
    public int getStartIndex(){
        //获取开始下标
        return  (pageNumber-1)*pageSize;
    }

    public List <T> getData() {
        return data;
    }

    public void setData(List <T> data) {
        this.data = data;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage() {

        //判断是否可以整除
        if (totalRecord % pageSize == 0) {
            this.totalPage =totalRecord/pageSize;
        }else {
            this.totalPage =totalRecord/pageSize+1;
        }
    }
}
