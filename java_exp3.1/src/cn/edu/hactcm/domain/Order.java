package cn.edu.hactcm.domain;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private String oid;
    private String ordertime;
    private Double total;
    private Integer state;
    private String address;
    private String name;
    private String telephone;
    private user user1;//表示订单属于那个用户
    private List <OrderItem> items = new ArrayList <>();

    public List <OrderItem> getItems() {
        return items;
    }

    public void setItems(List <OrderItem> items) {
        this.items = items;
    }

    public user getUser1() {
        return user1;
    }

    public void setUser1(user user1) {
        this.user1 = user1;
    }



    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(String ordertime) {
        this.ordertime = ordertime;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }


    public void save(Order order) {
    }
}
