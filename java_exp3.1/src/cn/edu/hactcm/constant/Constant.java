package cn.edu.hactcm.constant;

public interface Constant {
    //用户没有激活
    int USER_IS_NOT_ACTIVE=0;
    //用户已激活
    int USER_IS_ACTICE=1;

    //热门商品
    int PRODUCT_IS_HOT=1;

    //商品没有下架
    int PRODUCT_IS_UP=0;
    //商品逻辑下架
    int PRODUCT_IS_DOWN=1;
    //订单状态
    int ORDER_WEIFUKUAN=0;
    int ORDER_YIFUKUAN=1;
    int ORDER_YIFAHUO=2;
    int ORDER_YIWANCHENG=3;

}
