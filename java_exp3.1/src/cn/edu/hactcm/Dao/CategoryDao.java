package cn.edu.hactcm.Dao;

import cn.edu.hactcm.domain.Category;

import java.util.List;

public interface CategoryDao {
    List<Category> findAll();
}
