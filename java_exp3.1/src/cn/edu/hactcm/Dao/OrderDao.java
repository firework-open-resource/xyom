package cn.edu.hactcm.Dao;

import cn.edu.hactcm.domain.Order;
import cn.edu.hactcm.domain.OrderItem;
import cn.edu.hactcm.domain.PageBean;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface OrderDao {
    void saveItem(OrderItem oi);

    void save(Order order);

    int getTotalRecord(String uid);

    List<Order> findMyOrdersByPage(PageBean<Order> pb, String uid) throws InvocationTargetException, IllegalAccessException;

    Order getOrderById (String oid) throws Exception;
}
