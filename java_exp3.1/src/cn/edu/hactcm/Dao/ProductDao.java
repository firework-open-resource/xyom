package cn.edu.hactcm.Dao;

import cn.edu.hactcm.domain.PageBean;
import cn.edu.hactcm.domain.Product;

import java.util.List;

public interface ProductDao {
    List<Product> findNew();

    List<Product> findHot();

    Product getById(String pid);

    List<Product> findByPage(PageBean<Product> pb, String cid);

    int getTotalRecord(String cid);
}
