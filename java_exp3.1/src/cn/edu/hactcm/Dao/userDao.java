package cn.edu.hactcm.Dao;

import cn.edu.hactcm.domain.user;

public interface userDao {
    void save(user user1);

    user getByCode(String code);

    void update(user user1);

    user getByUsernameAndPwd(String username, String password);
}
