package cn.edu.hactcm.Dao.impl;

import cn.edu.hactcm.Dao.OrderDao;
import cn.edu.hactcm.Util.JDBCUtil;
import cn.edu.hactcm.Util.fengzhuangUtil;
import cn.edu.hactcm.domain.*;
import com.alibaba.druid.util.JdbcUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OrderDaoImpl implements OrderDao {
    //private JdbcTemplate template = new JdbcTemplate(JDBCUtil.getDataSource());
    private JdbcTemplate template;

    public void setTemplate (JdbcTemplate template) {
        this.template = template;
    }

    @Override
    /**
     * 保存订单项
     */
    public void saveItem(OrderItem oi) {
        String sql = "insert into orderitem value(?,?,?,?,?)";
        template.update(sql, new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.setString(1,oi.getItemid());
                preparedStatement.setInt(2,oi.getCount());
                preparedStatement.setDouble(3,oi.getSubtotal());
                preparedStatement.setString(4,oi.getProduct().getPid());
                preparedStatement.setString(5,oi.getOrder().getOid());
            }
        });

    }

    @Override
    /**
     * 保存订单
     */
    public void save(Order order) {
        String sql="insert into orders value(?,?,?,?,?,?,?,?)";

        template.update(sql, new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.setString(1,order.getOid());
                preparedStatement.setString(2,order.getOrdertime());
                preparedStatement.setDouble(3,order.getTotal());
                preparedStatement.setInt(4,order.getState());
                preparedStatement.setString(5,order.getAddress());
                preparedStatement.setString(6,order.getName());
                preparedStatement.setString(7,order.getTelephone());
                preparedStatement.setString(8,order.getUser1().getUid());
            }
        });

    }

    @Override
    /**
     * 获取我的订单的总条数
     */
    public int getTotalRecord(String uid) {
        String sql = "select count(*) from orders where uid=?";
        int tr = template.queryForObject(sql, Integer.class, uid);
        return tr;
    }

    @Override
    /**
     * 获取当前页数据
     */
    public List<Order> findMyOrdersByPage(PageBean<Order> pb, String uid) throws InvocationTargetException, IllegalAccessException {
        //查询所有订单
        String sql = "select * from orders where uid= '"+uid+"' order by ordertime desc limit "+pb.getStartIndex()+","+pb.getPageSize()+"";
        List <Map <String, Object>> orders = template.queryForList(sql);
        fengzhuangUtil fz = new fengzhuangUtil();
        List <Order> orders1 = fz.fenzhuagn1(orders);
        System.out.println(orders.getClass());
        //遍历订单集合，查询每一个订单的订单项
        for (Order order : orders1) {
            sql="select * from orderitem oi,product p where oi.pid=p.pid and oi.oid=?";
            List <Map <String, Object>> maplist = template.queryForList(sql, order.getOid());

            //遍历maplist 获取每一个订单项详情 封装成orderitem 将其加入当前订单的订单项列表中
            for (Map<String,Object> map:maplist
                 ) {
                //封装orderitem
                OrderItem oi = new OrderItem();
                BeanUtils.populate(oi,map);

                Product product = new Product();
                BeanUtils.populate(product,map);
                Category category = new Category();
                category.setCid(map.get("cid").toString());
                product.setCategory(category);
                oi.setProduct(product);
                //将orderitem翻入order的订单项列表
                order.getItems().add(oi);

            }

        }
        return orders1;
        //return  null;
    }

    @Override
    public Order getOrderById (String oid) {
        String sql="select * from orders where oid=?";
        final Order[] order = {new Order()};
        template.query(sql, new RowCallbackHandler() {
            @Override
            public void processRow (ResultSet resultSet) throws SQLException {
                order[0] = fengzhuangUtil.changeRsToOrder(resultSet);
            }
        },oid);
        Order order2=order[0];

        sql="select * from orderitem oi,product p where oi.pid=p.pid and oi.oid=?";


        template.query(sql, new RowCallbackHandler() {
            @Override
            public void processRow (ResultSet resultSet) throws SQLException {
                OrderItem orderItem = fengzhuangUtil.changeToOrderItem(resultSet);
                order2.getItems().add(orderItem);
            }
        },oid);

        return order2;
    }
}
