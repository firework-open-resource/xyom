package cn.edu.hactcm.Dao.impl;

import cn.edu.hactcm.Dao.userDao;
import cn.edu.hactcm.Util.JDBCUtil;
import cn.edu.hactcm.Util.date_deal;
import cn.edu.hactcm.domain.user;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class userDaoImpl implements userDao {
    //private JdbcTemplate template=new JdbcTemplate(JDBCUtil.getDataSource());
    private JdbcTemplate template;

    public void setTemplate (JdbcTemplate template) {
        this.template = template;
    }

    @Override
    public void save(user user1) {
        String sql="insert into user(uid,username,password," +
                "name,email,birthday,sex,state,code) value(?,?,?,?,?,?,?,?,?)";
        //boolean panding=false;
        template.update(sql, new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.setString(1, user1.getUid());
                preparedStatement.setString(2, user1.getUsername());
                preparedStatement.setString(3, user1.getPassword());
                preparedStatement.setString(4, user1.getName());
                preparedStatement.setString(5, user1.getEmail());
                preparedStatement.setString(6, user1.getBirthday());
                preparedStatement.setString(7, user1.getSex());
                preparedStatement.setInt(8, user1.getState());
                preparedStatement.setString(9, user1.getCode());
            }
        });
    }

    @Override
    //通过激活码获取用户
    public user getByCode(String code) {
        String sql="select * from user where code=? limit 1";
        user user1=null;
        try {

            user1 = template.queryForObject(sql, new BeanPropertyRowMapper <user>(user.class), code);
        //查询空值处理
        } catch (EmptyResultDataAccessException e) {
            //查询空值处理
            return null;
        }

        return user1;
    }

    @Override
    //更新用户
    public void update(user user1) {
        String sql = "update user set password = ? ,sex = ?, state = ? ,code = ? where uid = ?";
        template.update(sql, new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.setString(1, user1.getPassword());
                preparedStatement.setString(2,user1.getSex());
                preparedStatement.setInt(3, user1.getState());
                preparedStatement.setString(4, user1.getCode());
                preparedStatement.setString(5,user1.getUid());

            }
        });

    }

    @Override
    //用户登陆
    public user getByUsernameAndPwd(String username, String password) {
        String sql="select * from user where (username= ? or email=?) and password=? limit 1";
        user return_user=null;
        System.out.println(template);
        try {
            return_user = template.queryForObject(sql, new BeanPropertyRowMapper <user>(user.class),
                    username, username,password);

        } catch (EmptyResultDataAccessException e) {
            return  null;
        }

        return return_user;
    }
}
