package cn.edu.hactcm.Dao.impl;

import cn.edu.hactcm.Dao.CategoryDao;
import cn.edu.hactcm.Util.JDBCUtil;
import cn.edu.hactcm.domain.Category;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class CategoryDaoImpl implements CategoryDao {
    //private JdbcTemplate template=new JdbcTemplate(JDBCUtil.getDataSource());
    private JdbcTemplate template;

    public void setTemplate (JdbcTemplate template) {
        this.template = template;
    }

    @Override
    /**
     * 查询所有分类
     */
    public List<Category> findAll() {
        String sql="select * from category ";
        List lt=null;
        try {

            lt = template.queryForList(sql);

        } catch (EmptyResultDataAccessException e) {
            return  null;
        }
        return lt;
    }
}
