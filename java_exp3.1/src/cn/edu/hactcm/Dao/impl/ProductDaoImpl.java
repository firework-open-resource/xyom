package cn.edu.hactcm.Dao.impl;

import cn.edu.hactcm.Dao.ProductDao;
import cn.edu.hactcm.Util.JDBCUtil;
import cn.edu.hactcm.constant.Constant;
import cn.edu.hactcm.domain.PageBean;
import cn.edu.hactcm.domain.Product;
import com.sun.org.apache.xpath.internal.operations.Lt;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;

public class ProductDaoImpl implements ProductDao {
    //private JdbcTemplate template=new JdbcTemplate(JDBCUtil.getDataSource());
    private JdbcTemplate template;

    public void setTemplate (JdbcTemplate template) {
        this.template = template;
    }

    @Override
    public List<Product> findNew() {
        List lt=null;
        String sql="select * from product where pflag=? order by pdate desc limit 10";
        try {
            lt = template.queryForList(sql, Constant.PRODUCT_IS_UP);

        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        return lt;
    }

    @Override
    public List <Product> findHot() {
        List lt=null;
        String sql="select * from product where is_hot=? and pflag=? order by pdate desc limit 5";
        try {
            lt = template.queryForList(sql, Constant.PRODUCT_IS_HOT, Constant.PRODUCT_IS_UP);

        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        return lt;
    }

    @Override
    public Product getById(String pid) {
        String sql="select * from product where pid=? limit 1";
        Product product=null;
        try {
            product = template.queryForObject(sql, new BeanPropertyRowMapper <Product>(Product.class), pid);

        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        return product;
    }


    /**
     * 查询当前页数据
     */
    public List  findByPage(PageBean<Product> pb, String cid) {
        String sql="select * from product where cid=? and pflag=? order by pdate desc limit ?,? ";
//        Product product = template.queryForObject(sql, new BeanPropertyRowMapper <Product>(Product.class),
//                cid, Constant.PRODUCT_IS_UP,
//                pb.getStartIndex(), pb.getPageSize());
        //, pb.getStartIndex(), pb.getPageSize()
        System.out.println(pb.getStartIndex());
        System.out.println(pb.getPageSize());
        List maps = template.queryForList(sql, cid, Constant.PRODUCT_IS_UP,pb.getStartIndex(), pb.getPageSize());
        //template.queryForList(sql,new BeanPropertyRowMapper<>())
        return maps;
    }

    @Override
    /**
     * 查询总记录数
     */
    public int getTotalRecord(String cid) {
        String sql="select count(*) from product where cid=? and pflag=?";
        Integer num = template.queryForObject(sql, Integer.class, cid, Constant.PRODUCT_IS_UP);
        return num;
    }
}
