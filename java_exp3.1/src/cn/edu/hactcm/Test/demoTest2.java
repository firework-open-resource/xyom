package cn.edu.hactcm.Test;

public class demoTest2 extends father {
    private static int x=2;
    private Integer z=4;

    {
        x=6;
        z=6;
        System.out.println("child的构造代码块");

    }
    static {
        x=8;
        System.out.println("child的静态代码块");
    }
    public demoTest2(){
        z=10;
        x=10;
        System.out.println("child的构造函数");

    }
    public static void print(){
        System.out.println("子类静态方法");
    }

    public static void main (String[] args) {
        father father1 = new demoTest2();
        System.out.println(((demoTest2) father1).z);
        //方法是要被调用的时候 才执行
    }


}
class father {
    private static int x=2;//静态变量
    private Integer z=4;

    public static void print(){
        System.out.println("父类静态方法");
    }

    {
        x=6;
        z=6;
        System.out.println("father的构造代码块");

    }
    static {
        x=8;
        System.out.println("father的静态代码块");

    }
    public father(){
        z=10;
        x=10;
        System.out.println("father的构造函数");
    }


}
