package cn.edu.hactcm.Util;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
/*
懒汉单例模式
 */

//创建数据库连接池
public class JDBCUtil {
    private static DataSource ds;

    static {
        try {
            Properties pro = new Properties();
            InputStream is = JDBCUtil.class.getClassLoader().getResourceAsStream("druid.properties");
            pro.load(is);
            //DruidDataSourceFactory ddsf = new DruidDataSourceFactory();
            //在这里出现错误，ds创建失败
            ds = DruidDataSourceFactory.createDataSource(pro);
            Connection connection = ds.getConnection();

            System.out.println(connection);


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static DataSource getDataSource(){
        return ds;
    }

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}
