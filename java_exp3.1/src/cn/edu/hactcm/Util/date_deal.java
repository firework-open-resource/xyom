package cn.edu.hactcm.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class date_deal {
    private  long date;

    public date_deal() {
        this.date = new Date().getTime();
    }

    //获取时间
    public  String getDateFormat(){
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sd.format(date);
    }


    //获取用户，帖子主题，评论的id
    public String obtain_id(String type) {
        return type+date;
    }

    //自动生成一个图片id：P+时间戳+三位随机数
    public String getPicId() {
        StringBuffer sb = new StringBuffer();
        sb.append("P");
        sb.append(date);
        Random rd = new Random();
        for (int i = 0; i < 3; i++) {
            sb.append(rd.nextInt(10));
        }
        return  sb.toString();
    }
    public String getId() {
        StringBuffer sb = new StringBuffer();
        //sb.append("P");
        sb.append(date);
        Random rd = new Random();
        for (int i = 0; i < 5; i++) {
            sb.append(rd.nextInt(10));
        }
        return  sb.toString();
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getDate() {
        return date+"";
    }
}
