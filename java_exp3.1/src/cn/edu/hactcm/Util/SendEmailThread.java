package cn.edu.hactcm.Util;

public class SendEmailThread extends Thread {
    private String subject;
    private String body;
    private String tos;
    private String[] file;
    public SendEmailThread(String subject, String body, String tos, String[] files){
        this.subject=subject;
        this.body=body;
        this.tos=tos;
        this.file=files;

    }
    @Override
    public void run () {
        mailUtil.sendEmail(subject,body,tos,file);



    }
}
