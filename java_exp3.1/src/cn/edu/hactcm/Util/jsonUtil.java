package cn.edu.hactcm.Util;

import cn.edu.hactcm.domain.Category;
import net.sf.json.JSONArray;

import java.util.List;

public class jsonUtil {
    //list转json
    public String listToJson(List <Category> list) {
        JSONArray jsonArray = JSONArray.fromObject(list);
        return jsonArray.toString();
    }
    //json转list
    public String jsonToList(Object[] objects) {
        JSONArray jsonArray = JSONArray.fromObject(objects);
        return jsonArray.toString();
    }
}
