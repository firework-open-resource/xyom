package cn.edu.hactcm.Util;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

public class BeanFactory {

    public static Object getBean(String id) {
        try {
            //取得document对象
            Document doc = new SAXReader().read(BeanFactory.class.getClassLoader().getResourceAsStream("bean.xml"));

            //调用api selectSingleNode（表达式）
            Element beanEle = (Element) doc.selectSingleNode("//bean[@id='"+id+"']");

            //获取元素的class属性
            String aClass = beanEle.attributeValue("class");

            //通过反射返回实现类的对象
            Object newInstance = Class.forName(aClass).newInstance();
            return newInstance;

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("获取bean失败");
        }
        return null;
    }
}
