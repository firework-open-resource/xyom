package cn.edu.hactcm.Util;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class BeanFactory2 {

    public static Object getBean(String id) {
        try {
            ClassPathXmlApplicationContext ca = new ClassPathXmlApplicationContext("bean.xml");
            return ca.getBean(id);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("获取bean失败");
        }
        return null;
    }
}
