package cn.edu.hactcm.Util;

import cn.edu.hactcm.domain.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class fengzhuangUtil {
    public List<Order> fenzhuagn1(List <Map <String, Object>> li){
        List<Order> lo=new ArrayList <>();
        for (int i = 0; i < li.size(); i++) {
            Order order = new Order();
            if (li.get(i).get("oid")!=null)
                order.setOid(li.get(i).get("oid").toString());
            if (li.get(i).get("ordertime")!=null)
                order.setOrdertime(li.get(i).get("ordertime").toString());
            if (li.get(i).get("total")!=null)
                order.setTotal((Double) li.get(i).get("total"));
            if (li.get(i).get("state")!=null)
                order.setState((int)li.get(i).get("state"));
            if (li.get(i).get("address")!=null)
                order.setAddress(li.get(i).get("address").toString());
            if (li.get(i).get("name")!=null)
                order.setName(li.get(i).get("name").toString());
            if (li.get(i).get("telephone")!=null)
                order.setTelephone(li.get(i).get("telephone").toString());

            user user1 = new user();
            if (li.get(i).get("uid")!=null)
                user1.setUid(li.get(i).get("uid").toString());
            order.setUser1(user1);
            lo.add(order);

        }
        return lo;
    }

    //把一个resultSet封装为一个order
    public static Order  changeRsToOrder(ResultSet resultSet) throws SQLException {
        Order order=new Order();
        order.setOid(resultSet.getString("oid"));
        order.setOrdertime(resultSet.getString("ordertime"));
        order.setTotal(resultSet.getDouble("total"));
        order.setState(resultSet.getInt("state"));
        order.setAddress(resultSet.getString("address"));
        order.setName(resultSet.getString("name"));
        order.setTelephone(resultSet.getString("telephone"));

        user user1=new user();
        user1.setUid(resultSet.getString("uid"));
        order.setUser1(user1);

        return order;
    }

    //把获取到的订单项信息进行封装
    public static OrderItem changeToOrderItem(ResultSet resultSet) throws SQLException {

        /**
         * 封装信息
         *       String itemid;
         *      Integer count;
         *      Double subtotal;
         *      Product product ;//表示包含那个商品
         *      Order order
         */

        OrderItem orderItem=new OrderItem();
        orderItem.setItemid(resultSet.getString("itemid"));
        orderItem.setCount(resultSet.getInt("count"));
        orderItem.setSubtotal(resultSet.getDouble("subtotal"));

        //封装Product
        Product product = new Product();
        product.setPid(resultSet.getString("pid"));
        product.setPname(resultSet.getString("pname"));
        product.setMarket_price(resultSet.getDouble("market_price"));
        product.setShop_price(resultSet.getDouble("shop_price"));
        product.setPimage(resultSet.getString("pimage"));
        product.setPdate(resultSet.getString("pdate"));
        product.setIs_hot(resultSet.getInt("is_hot"));
        product.setPdesc(resultSet.getString("pdesc"));
        product.setPflag(resultSet.getInt("pflag"));

        Category category=new Category();
        category.setCid(resultSet.getString("cid"));
        product.setCategory(category);
        orderItem.setProduct(product);

        Order order=new Order();
        order.setOid(resultSet.getString("oid"));
        orderItem.setOrder(order);

        return orderItem;


    }

}
