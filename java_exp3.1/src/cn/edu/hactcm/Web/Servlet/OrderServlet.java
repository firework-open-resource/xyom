package cn.edu.hactcm.Web.Servlet;

import cn.edu.hactcm.Service.OrderService;
import cn.edu.hactcm.Util.BeanFactory2;
import cn.edu.hactcm.Util.date_deal;
import cn.edu.hactcm.Web.Servlet.Base.BaseServlet;
import cn.edu.hactcm.constant.Constant;
import cn.edu.hactcm.domain.*;
import sun.misc.Request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 订单模块
 */
@WebServlet("/OrderServlet")
public class OrderServlet extends BaseServlet {

    /**
     *保存订单
     * @param req
     * @param resp
     * @return
     */
    public String save(HttpServletRequest req, HttpServletResponse resp) {
        try {


            //从session里面获取user
            user user1 = (user) req.getSession().getAttribute("user");
            if (user1 == null) {
                req.setAttribute("msg", "请先登录！");
                return "/jsp/msg.jsp";
            }
            //创建购物车
            Cart cart = (Cart) req.getSession().getAttribute("cart");

            //封装订单对象
            /**
             * 创建对象
             * 设置oid ordertime total state user items
             */
            Order order = new Order();
            date_deal dd = new date_deal();
            order.setOid(dd.getDate());
            order.setOrdertime(dd.getDateFormat());
            order.setTotal(cart.getTotal());
            order.setState(Constant.ORDER_WEIFUKUAN);
            order.setUser1(user1);

            for (CartItem ci : cart.getCartItems()) {
                OrderItem oi = new OrderItem();
                date_deal ddeal = new date_deal();
                oi.setItemid(ddeal.getId());
                oi.setCount(ci.getCount());
                oi.setSubtotal(ci.getSubtotal());
                oi.setProduct(ci.getProduct());
                oi.setOrder(order);

                order.getItems().add(oi);
            }

            //调用orderservice完成保存操作
            OrderService os = (OrderService) BeanFactory2.getBean("OrderService");
            os.save(order);
            //请求转发到order_info.jsp
            cart.ClearCart();
            req.setAttribute("bean", order);

            return "/jsp/order_info.jsp";
        } catch (Exception e) {
            e.printStackTrace();
            req.setAttribute("msg","订单生成失败！");
            return "/jsp/msg.jsp";
        }

    }

    /**
     * 获取订单通过订单ID
     * @param req
     * @param resp
     * @return
     */
    public String getOrderById (HttpServletRequest req, HttpServletResponse resp) {
        //获取oid
        String oid = req.getParameter("oid");
        //调用service，查询单个订单
        OrderService orderService=(OrderService)BeanFactory2.getBean("OrderService");
        Order order= null;
        try {
            order = orderService.getOrderById(oid);
        } catch (Exception e) {
            e.printStackTrace();
            req.setAttribute("msg","查询订单错误！");
            return "/jsp/msg.jsp";
        }
        //请求转发
        req.setAttribute("bean",order);
        return "/jsp/order_info.jsp";

    }

    /**
     * 我的订单
     */
    public String findMyOrderGetByPage(HttpServletRequest req, HttpServletResponse resp) {
        try {

        //pageNumber 设置pageSize，获取当前用户的id
        int pageNumber =Integer.parseInt( req.getParameter("pageNumber"));
        int pageSize=3;
        user user1=(user) req.getSession().getAttribute("user");
        if (user1==null){
            //表示未登录
            req.setAttribute("msg","请先登陆");
            return "/jsp/msg.jsp";
        }
        String uid = user1.getUid();

        //调用service，完成分页查询操作，返回pagebean

        OrderService os= (OrderService)BeanFactory2.getBean("OrderService");
        PageBean <Order> bean = os.findMyOrderGetByPage(pageNumber, pageSize, uid);

        //将pagebean放入request域中，请求转发到 order_list.jsp
            //System.out.println(bean);

        req.setAttribute("pb",bean);
        return "/jsp/order_list.jsp";
        }catch (Exception e){
            e.printStackTrace();
            req.setAttribute("msg","进入我的订单失败");
            return "/jsp/msg.jsp";
        }
    }
}
