package cn.edu.hactcm.Web.Servlet.Base;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

@WebServlet("/BaseServlet")
public class BaseServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) {
        try {
            //获取方法名称
            String nName = req.getParameter("method");
            if (nName == null || nName.trim().length() == 0) {
                nName = "index";
            }
            //获取方法对象
            final Method method = this.getClass().getMethod(nName, HttpServletRequest.class, HttpServletResponse.class);
            //让方法执行，接受返回值
            String path = (String) method.invoke(this, req, resp);
            //path=req.getContextPath()+path;
            //判断返回值是否为空，若不为空统一处理请求转发
            if (path != null) {
                req.getRequestDispatcher(path).forward(req,resp);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    public String index(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html;charset=utf-8");
        resp.getWriter().println("请不要捣乱");
        return null;
    }

}

