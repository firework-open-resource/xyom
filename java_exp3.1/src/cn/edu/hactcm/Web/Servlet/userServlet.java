package cn.edu.hactcm.Web.Servlet;

import cn.edu.hactcm.Service.impl.userServiceImpl;
import cn.edu.hactcm.Service.userService;
import cn.edu.hactcm.Util.BeanFactory2;
import cn.edu.hactcm.Util.date_deal;
import cn.edu.hactcm.Web.Servlet.Base.BaseServlet;
import cn.edu.hactcm.constant.Constant;
import cn.edu.hactcm.domain.user;
import sun.misc.Request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/userServlet")
public class userServlet extends BaseServlet {

    //跳转到注册页面
    public String regeditUI(HttpServletRequest req, HttpServletResponse resp) {
        return "/jsp/regedit_jsp.jsp";
    }
    //跳转到登陆页面
    public String loginUI(HttpServletRequest req, HttpServletResponse resp) {
        return "/jsp/login_jsp.jsp";
    }
    //注册
    public String regedit(HttpServletRequest req, HttpServletResponse resp) {
        try {
            //封装对象
            user user = new user();
            final date_deal dateDeal = new date_deal();
            user.setUid(dateDeal.obtain_id("U"));
            user.setUsername(req.getParameter("username"));
            user.setPassword(req.getParameter("password"));
            user.setEmail(req.getParameter("email"));
            user.setSex(req.getParameter("sex"));
            user.setBirthday(req.getParameter("birthday"));
            user.setName(req.getParameter("name"));
            //激活状态，激活码
            user.setState(Constant.USER_IS_NOT_ACTIVE);
            user.setCode(dateDeal.getDate());
            //System.out.println(user.toString());
            userService userService = (userService) BeanFactory2.getBean("userService");
            userService.regedit(user);

            req.setAttribute("msg","恭喜你注册成功，请登录邮箱完成激活");

        } catch (Exception e) {
            e.printStackTrace();
            req.setAttribute("msg","注册失败！");
            return "/jsp/msg.jsp";
        }


        return "/jsp/msg.jsp";
    }

    //激活


    public String active(HttpServletRequest req, HttpServletResponse resp) {
        //接受code
        String code = req.getParameter("code");
        //调用service，完成激活，返回值user1
        userService userService =(userService)BeanFactory2.getBean("userService");
        user user1 = userService.active(code);

        //判断user1，生成不同的提示信息
        if (user1 == null) {
            //激活失败
            req.setAttribute("msg","请重新激活或者重新注册");
            return "/jsp/msg.jsp";
        }
        //激活成功
        req.setAttribute("msg","恭喜你，激活成功，可以登陆了");
        return "/jsp/msg.jsp";


    }
    //登陆

    public String login(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //获取用户名及密码
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        //调用service完成登陆，返回值user1
        userService userService =(userService)BeanFactory2.getBean("userService");
        user user1 = userService.login(username, password);

        //判断user1，根据结果生成提示信息
        if (user1 == null) {
            //用户名与密码不匹配
            req.setAttribute("msg","用户名或密码不匹配");
            return "/jsp/login_jsp.jsp";
        }
        //如果匹配，判断是否激活
        if (Constant.USER_IS_ACTICE != user1.getState()) {
            req.setAttribute("msg", "请先激活，再登陆");
            return "/jsp/msg.jsp";
        }
        //登陆成功，保存用户信息
        req.getSession().setAttribute("user", user1);
        resp.sendRedirect(req.getContextPath()+"/index2.jsp");
        return null;
    }

    //退出

    public String logout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.getSession().invalidate();
        resp.sendRedirect(req.getContextPath()+"/index2.jsp");
        return null;
    }
}
