package cn.edu.hactcm.Web.Servlet;

import cn.edu.hactcm.Service.ProductService;
import cn.edu.hactcm.Service.impl.ProductServiceImpl;
import cn.edu.hactcm.Util.BeanFactory2;
import cn.edu.hactcm.Web.Servlet.Base.BaseServlet;
import cn.edu.hactcm.domain.PageBean;
import cn.edu.hactcm.domain.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ProductServlet")
public class ProductServlet extends BaseServlet {
    //通过pid查询出来商品的详细信息
    public String getById(HttpServletRequest req, HttpServletResponse resp) {
        //获取pid
        String pid = req.getParameter("pid");

        //调用service方法，返回值Product
        ProductService productService =(ProductService) BeanFactory2.getBean("ProductService");
        Product pt = productService.getById(pid);

        req.setAttribute("bean",pt);

        return "/jsp/product_info.jsp";
    }

    /**
     * 分类商品展示
     */
    public String findByPage(HttpServletRequest req, HttpServletResponse resp) {

        //获取pageNumber cid 设置pagesize
        int pageNumber=1;
        try {
            pageNumber = Integer.parseInt(req.getParameter("pageNumber"));
        } catch (NumberFormatException e) {

        }
        int pageSize=10;
        String cid = req.getParameter("cid");

        //调用service，分页查询参数：三个  ，返回值pagebean
        ProductService productService =(ProductService) BeanFactory2.getBean("ProductService");
        PageBean<Product> bean = productService.findByPage(pageNumber, pageSize, cid);
        bean.setTotalPage();
        //将pagebean放入request域中，请求转发到 product_list.jsp

        req.setAttribute("pb",bean);

        return "/jsp/product_list.jsp";
    }
}
