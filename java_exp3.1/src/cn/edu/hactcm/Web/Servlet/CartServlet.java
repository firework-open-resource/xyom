package cn.edu.hactcm.Web.Servlet;

import cn.edu.hactcm.Service.ProductService;
import cn.edu.hactcm.Util.BeanFactory2;
import cn.edu.hactcm.Web.Servlet.Base.BaseServlet;
import cn.edu.hactcm.domain.Cart;
import cn.edu.hactcm.domain.CartItem;
import cn.edu.hactcm.domain.Product;
import cn.edu.hactcm.domain.user;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/*
购物车是一次性的，再次登陆已经没有了，是因为：程序他购物车存在Session里面
 */
@WebServlet("/CartServlet")
public class CartServlet extends BaseServlet {
    /**
     * 添加到购物车
     * @param req
     * @param resp
     * @return
     */
    public String addToCart(HttpServletRequest req, HttpServletResponse resp) {
        try {
            user user1 = (user) req.getSession().getAttribute("user");
            if (user1 == null) {
                req.setAttribute("msg", "请先登录！");
                return "/jsp/msg.jsp";
            }

            //获取pid，count
            String pid = req.getParameter("pid");
            int count = Integer.parseInt(req.getParameter("count"));

            //工厂实例化一个productService对象
            ProductService productService = (ProductService) BeanFactory2.getBean("ProductService");
            //查询pid对象的商品信息
            Product product = productService.getById(pid);

            //创建商品项（CartItem）
            CartItem cartItem = new CartItem(product, count);

            //将商品项加入购物车（Cart）
            //获取购物车
            Cart cart = getCart(req);
            cart.addToCart(cartItem);
            resp.sendRedirect(req.getContextPath()+"/jsp/Cart.jsp");
        } catch (Exception e) {
            e.printStackTrace();
            req.setAttribute("msg","加入购物车失败");
            return "/jsp/msg.jsp";
        }
        return null;
    }

    /**
     * 获取购物车方法
     * @param req
     * @return
     */
    private Cart getCart(HttpServletRequest req) {
        Cart cart= (Cart)req.getSession().getAttribute("cart");
        if (cart == null) {
            cart=new Cart();

            req.getSession().setAttribute("cart",cart);
        }
        return cart;

    }

    public String showCart(HttpServletRequest req, HttpServletResponse resp){
        user user1 = (user) req.getSession().getAttribute("user");
        if (user1 == null) {
            req.setAttribute("msg", "请先登录！");
            return "/jsp/msg.jsp";
        }
        try {
            resp.sendRedirect(req.getContextPath()+"/jsp/Cart.jsp");
        } catch (IOException e) {
            e.printStackTrace();
            req.setAttribute("msg","查询失败");
            return "/jsp/msg.jsp";
        }
        return null;

    }
    /**
     * 删除购物项从购物车里面
     */
    public String remove(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //获取商品的pid
        String pid = req.getParameter("pid");
        //获取购物车，执行移除
        getCart(req).removeFromCart(pid);
        //重定向
        resp.sendRedirect(req.getContextPath()+"/jsp/Cart.jsp");
        return null;
    }
    /**
     * 清空购物车
     */
    public String clear(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        //获取购物车，执行移除
        getCart(req).ClearCart();
        //重定向
        resp.sendRedirect(req.getContextPath()+"/jsp/Cart.jsp");
        return null;
    }
}
