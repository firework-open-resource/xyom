package cn.edu.hactcm.Web.Servlet;

import cn.edu.hactcm.Service.ProductService;
import cn.edu.hactcm.Service.impl.ProductServiceImpl;
import cn.edu.hactcm.Util.BeanFactory2;
import cn.edu.hactcm.Web.Servlet.Base.BaseServlet;
import cn.edu.hactcm.domain.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/indexServlet")
public class indexServlet extends BaseServlet {
    @Override
    public String index(HttpServletRequest req, HttpServletResponse resp) {
        //调用productService查询最新商品和热门商品
        ProductService productService = (ProductService) BeanFactory2.getBean("ProductService");
        List<Product> hotList=productService.findHot();
        List <Product> newList = productService.findNew();

        //将连个list都放入request里面
        req.setAttribute("hList", hotList);
        req.setAttribute("nList",newList);
        return "/index.jsp";
    }
}
