package cn.edu.hactcm.Web.Servlet;

import cn.edu.hactcm.Service.CategoryService;
import cn.edu.hactcm.Service.impl.CategoryServiceImpl;
import cn.edu.hactcm.Util.BeanFactory2;
import cn.edu.hactcm.Web.Servlet.Base.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 前台分类模块
 */
@WebServlet("/categoryServlet")
public class categoryServlet extends BaseServlet {


    public String findAll(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //设置响应编码
        resp.setContentType("text/html;charset=utf-8");
        //调用service
        CategoryService categoryService = (CategoryService) BeanFactory2.getBean("CategoryService");
        String value = categoryService.findAll();

        //将字符串返回浏览器
        resp.getWriter().println(value);
        return null;
    }
}
