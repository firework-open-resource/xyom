package cn.edu.hactcm.Web.filter;

import cn.edu.hactcm.domain.user;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "PrivilegeFilter", value = {"/jsp/Cart.jsp","/jsp/order_info.jsp","/jsp/order_list.jsp"})
public class PrivilegeFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req1, ServletResponse resp1, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req=(HttpServletRequest)req1;
        HttpServletResponse resp=(HttpServletResponse)resp1;

        user user1 = (user) req.getSession().getAttribute("user");
        if (user1 == null) {
            req.setAttribute("msg","您还尚未登陆，请先登录！");
            req.getRequestDispatcher("/jsp/msg.jsp").forward(req,resp);
            return;
        }
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) {

    }

}
