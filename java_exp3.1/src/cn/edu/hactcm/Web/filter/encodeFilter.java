package cn.edu.hactcm.Web.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(filterName = "encodeFilter",value = "/*")
public class encodeFilter implements Filter {
    public void destroy() {
    }
    //过滤器
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        chain.doFilter(req, resp);
        resp.setCharacterEncoding("utf-8");
    }

    public void init(FilterConfig config) {

    }

}
