<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/functions" %>

<%--
  Created by IntelliJ IDEA.
  User: wangshibo
  Date: 2020/4/2
  Time: 10:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="author" content="order by dede58.com"/>
  <title>xueyuan商城</title>
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style.css">
  <script src="http://libs.baidu.com/jquery/2.1.4/jquery.min.js"></script>
</head>
<body>
<!-- start header -->
<%@include file="/jsp/head.jsp"%>

<!-- start banner_y -->

<!-- end banner -->

<!-- start danpin -->
<div class="danpin center">

  <div class="biaoti center">热门商品</div>
  <div class="main center">
      <c:forEach items="${hList}" var="vr">

        <div class="mingxing fl">
          <div class="sub_mingxing"><a href="<%=request.getContextPath()%>/ProductServlet?method=getById&pid=${vr.pid}"><img src="<%=request.getContextPath()%>${vr.pimage}" alt=""></a></div>
          <div class="pinpai"><a href="<%=request.getContextPath()%>/ProductServlet?method=getById&pid=${vr.pid}">${vr.pname}</a></div>
          <div class="youhui">${f:substring(vr.pdesc,0,16)}...</div>
          <div class="jiage">￥${vr.shop_price}</div>
        </div>

      </c:forEach>

    <div class="clear"></div>
  </div>
</div>

<div class="peijian w">
  <div class="biaoti center">最新商品</div>
  <div class="main center">
    <div class="content">
      <%--<div class="remen fl"><a href=""><img src="./image/peijian1.jpg"></a></div>--%>
        <c:forEach items="${nList}" var="is" begin="0" end="4">
          <div class="remen fl">
            <div class="xinpin"><span>新品</span></div>
            <div class="tu"><a href="<%=request.getContextPath()%>/ProductServlet?method=getById&pid=${is.pid}"><img src="<%=request.getContextPath()%>${is.pimage}"></a></div>
            <div class="miaoshu"><a href="<%=request.getContextPath()%>/ProductServlet?method=getById&pid=${is.pid}">${is.pname}</a></div>
              <div class="pingjia">${f:substring(is.pdesc,0,16)}...</div>
              <div class="jiage">￥${is.shop_price}</div>
            <%--<div class="piao">--%>
              <%--<a href="">--%>
                <%--<span>发货速度很快！很配小米6！</span>--%>
                <%--<span>来至于mi狼牙的评价</span>--%>
              <%--</a>--%>
            <%--</div>--%>
          </div>
        </c:forEach>
      <div class="clear"></div>
    </div>

    <div class="content">

        <c:forEach items="${nList}" var="is" begin="5" end="9">
            <div class="remen fl">
                <div class="xinpin"><span>新品</span></div>
                <div class="tu"><a href="<%=request.getContextPath()%>/ProductServlet?method=getById&pid=${is.pid}"><img src="<%=request.getContextPath()%>${is.pimage}"></a></div>
                <div class="miaoshu"><a href="<%=request.getContextPath()%>/ProductServlet?method=getById&pid=${is.pid}">${is.pname}</a></div>
                <div class="pingjia">${f:substring(is.pdesc,0,16)}...</div>
                <div class="jiage">￥${is.shop_price}</div>

            </div>
        </c:forEach>

      <div class="clear"></div>
    </div>
  </div>
</div>
<footer class="mt20 center">
  <div class="mt20">小米商城|MIUI|米聊|多看书城|小米路由器|视频电话|小米天猫店|小米淘宝直营店|小米网盟|小米移动|隐私政策|Select Region</div>
  <div>©mi.com 京ICP证110507号 京ICP备10046444号 京公网安备11010802020134号 京网文[2014]0059-0009号</div>
  <div>违法和不良信息举报电话：185-0130-1238，本网站所列数据，除特殊说明，所有数据均出自我司实验室测试</div>
</footer>
</body>
</html>
