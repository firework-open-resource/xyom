<%--
  Created by IntelliJ IDEA.
  User: wangshibo
  Date: 2020/4/17
  Time: 11:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- start header -->
<header>
    <div class="top center">
        <div class="left fl">
            <ul>
                <li><a href="http://www.mi.com/" target="_blank">xueyuan商城</a></li>
                <li>|</li>
                <li><a href="">MIUI</a></li>
                <li>|</li>
                <li><a href="">米聊</a></li>
                <li>|</li>
                <li><a href="">游戏</a></li>
                <li>|</li>
                <li><a href="">多看阅读</a></li>
                <li>|</li>
                <li><a href="">云服务</a></li>
                <li>|</li>
                <li><a href="">金融</a></li>
                <li>|</li>
                <li><a href="">小米商城移动版</a></li>
                <li>|</li>
                <li><a href="">问题反馈</a></li>
                <li>|</li>
                <li><a href="">Select Region</a></li>
                <div class="clear"></div>
            </ul>
        </div>
        <div class="right fr">
            <div class="gouwuche fr"><a href="<%=request.getContextPath()%>/CartServlet?method=showCart">购物车</a></div>
            <div class="fr">
                <ul>
                    <c:if test="${empty user}">
                        <li><a href="<%=request.getContextPath()%>/userServlet?method=loginUI">登录</a></li>
                        <li>|</li>
                        <li><a href="<%=request.getContextPath()%>/userServlet?method=regeditUI">注册</a></li>
                    </c:if>
                    <c:if test="${not empty user}">
                        <li>${user.name}:你好</li>
                        <li><a href="<%=request.getContextPath()%>/OrderServlet?method=findMyOrderGetByPage&pageNumber=1">我的订单</a></li>
                        <li>|</li>
                        <li><a href="<%=request.getContextPath()%>/userServlet?method=logout">退出</a></li>
                    </c:if>

                    <li>|</li>
                    <li><a href="javascript:void(0)">消息通知</a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</header>
<!--end header -->

<!-- start banner_x -->
<div class="banner_x center">
    <a href="<%=request.getContextPath()%>/index2.html"><div class="logo fl"></div></a>

    <div class="nav fl">
        <ul id="c_ul">
            <li><a href="<%=request.getContextPath()%>/index2.jsp">首页</a></li>
        </ul>
    </div>
    <div class="search fr">
        <form action="" method="post">
            <div class="text fl">
                <input type="text" class="shuru"  placeholder="小米6&nbsp;小米MIX现货">
            </div>
            <div class="submit fl">
                <input type="submit" class="sousuo" value="搜索"/>
            </div>
            <div class="clear"></div>
        </form>
        <div class="clear"></div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        //JQuery操作
        //请求的页面
        $.post("${pageContext.request.contextPath}/categoryServlet", {"method": "findAll"},
            function (obj) {
            //alert(obj)
                //遍历json列表，获取每一个分类，包装成li标签，插入到ul内部
                $(obj).each(function () {
                    //alert(this.cname);
                    $("#c_ul").append("<li><a href='${pageContext.request.contextPath}/ProductServlet?method=findByPage&pageNumber=1&cid="+this.cid+"'>"+this.cname+"</a></li>")

                });

            },"json");

    });
</script>
