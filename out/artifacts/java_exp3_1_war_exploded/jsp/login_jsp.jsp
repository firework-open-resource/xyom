<%--
  Created by IntelliJ IDEA.
  User: wangshibo
  Date: 2020/2/21
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="author" content="order by dede58.com"/>
    <title>会员登录</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/login.css">

</head>
<body>
<!-- login -->
<div class="top center">
    <div class="logo center">
        <a href="<%=request.getContextPath()%>/index.html" target="_blank"><img src="<%=request.getContextPath()%>/image/mistore_logo.png" alt=""></a>
    </div>
</div>
<form  method="post" action="<%=request.getContextPath()%>/userServlet" class="form center">
    <div class="login">
        <div class="login_center">
            <div class="login_top">
                <input type="hidden" name="method" value="login">
                <div class="left fl">会员登录</div>
                <div class="right fr">您还不是我们的会员？<a href="<%=request.getContextPath()%>/userServlet?method=regeditUI" target="_self">立即注册</a></div>
                <div class="clear"></div>
                <div class="xian center"></div>
            </div>
            <div class="login_main center">
                <div class="username">用户名:&nbsp;<input class="shurukuang" type="text" name="username" placeholder="请输入用户名或者邮箱"/></div>
                <div class="username">密&nbsp;&nbsp;&nbsp;&nbsp;码:&nbsp;<input class="shurukuang" type="password" name="password" placeholder="请输入你的密码"/></div>
                <div class="username">
                    <div class="left fl">验证码:&nbsp;<input class="yanzhengma" type="text" name="username" placeholder="请输入验证码"/></div>
                    <div class="right fl"><img src="<%=request.getContextPath()%>/image/yanzhengma.jpg"></div>
                    <div class="clear"></div>
                </div>
                <span style="color: red">${msg}</span>
            </div>
            <div class="login_submit">
                <input class="submit" type="submit" name="submit" value="立即登录" >
            </div>

        </div>
    </div>
</form>
<footer>
    <div class="copyright">简体 | 繁体 | English | 常见问题</div>
    <div class="copyright">小米公司版权所有-京ICP备10046444-<img src="<%=request.getContextPath()%>/image/ghs.png" alt="">京公网安备11010802020134号-京ICP证110507号</div>

</footer>
</body>
</html>

