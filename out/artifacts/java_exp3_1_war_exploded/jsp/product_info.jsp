<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: wangshibo
  Date: 2020/4/18
  Time: 0:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="author" content="order by dede58.com"/>
    <title>小米6立即购买-小米商城</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style.css">
    <script src="http://libs.baidu.com/jquery/2.1.4/jquery.min.js"></script>
</head>
<body>
<!-- start header -->
<%@include file="head.jsp"%>
<!--end header -->

<!-- start banner_x -->

<!-- end banner_x -->


<!-- xiangqing -->
<form action="<%=request.getContextPath()%>/CartServlet" method="post" id="form1">
    <input type="hidden" name="method" value="addToCart">
    <input type="hidden" name="pid" value="${bean.pid}">
    <div class="xiangqing">
        <div class="neirong w">
            <div class="xiaomi6 fl">小米6</div>
            <nav class="fr">
                <li><a href="">概述</a></li>
                <li>|</li>
                <li><a href="">变焦双摄</a></li>
                <li>|</li>
                <li><a href="">设计</a></li>
                <li>|</li>
                <li><a href="">参数</a></li>
                <li>|</li>
                <li><a href="">F码通道</a></li>
                <li>|</li>
                <li><a href="">用户评价</a></li>
                <div class="clear"></div>
            </nav>
            <div class="clear"></div>
        </div>
    </div>

    <div class="jieshao mt20 w">
        <div class="left fl"><img src="<%=request.getContextPath()%>${bean.pimage}"></div>
        <div class="right fr">
            <div class="h3 ml20 mt20">${bean.pname}</div>
            <div class="jianjie mr40 ml20 mt10">${bean.pdesc}</div>
            <div class="jiage ml20 mt10">价格：${bean.shop_price}元</div>


            <div class="ft20 ml20 mt20">购买数量：</div>
            <div class="xzbb ml20 mt10">
                <input type="number" style="width: 190px;height: 35px;" name="count" value="1" id="buy_num" onkeyup="heji()" min="1" max="10000">

            </div>
            <div class="xqxq mt20 ml20">
                <div class="top1 mt10">
                    <div class="left1 fl">${bean.pname}</div>
                    <div class="right1 fr">${bean.shop_price}</div>
                    <div class="clear"></div>
                </div>
                <div class="bot mt20 ft20 ftbc" >总计：<span id="zongji">${bean.shop_price}</span>元</div>
            </div>
            <div class="xiadan ml20 mt20">
                <input class="jrgwc"  type="button" name="jrgwc"  value="立即选购" />
                <input class="jrgwc" type="button" name="jrgwc" onclick="subForm()" value="加入购物车" />

            </div>
        </div>
        <div class="clear"></div>
        <script>
            function heji() {

                var buy_num = document.getElementById("buy_num");
                var zongji_text=buy_num.value*${bean.shop_price}
                var zongji=document.getElementById("zongji");
                zongji.innerText=zongji_text;
            }

            function subForm() {
                document.getElementById("form1").submit();

            }
        </script>
    </div>
</form>
<!-- footer -->
<footer class="mt20 center">

    <div class="mt20">多看书城|小米路由器|视频电话|小米天猫店|小米淘宝直营店|小米网盟|小米移动|隐私政策|Select Region</div>
    <div>©mi.com 京ICP证110507号 京ICP备10046444号 京公网安备11010802020134号 京网文[2014]0059-0009号</div>
    <div>违法和不良信息举报电话：185-0130-1238，本网站所列数据，除特殊说明，所有数据均出自我司实验室测试</div>

</footer>

</body>
</html>
