<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: wangshibo
  Date: 2020/4/19
  Time: 10:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="author" content="order by dede58.com"/>
    <title>我的购物车-xueyuan商城</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style.css">
    <script src="http://libs.baidu.com/jquery/2.1.4/jquery.min.js"></script>
</head>
<body>
<!-- start header -->
<%@include file="head.jsp"%>
<!-- start banner_x -->

<div class="xiantiao"></div>

<div class="gwcxqbj">
    <div class="gwcxd center">
        <c:if test="${empty cart||empty cart.cartItems}">
            <h4>购物车空空如也，请先去逛逛去吧.........</h4>
        </c:if>
        <c:if test="${not empty cart.cartItems}">
        <%--标题栏--%>
        <div class="top2 center">
            <div class="sub_top fl">
                <%--<input type="checkbox" value="quanxuan" class="quanxuan" />全选--%>
            </div>
            <div class="sub_top fl">商品名称</div>
            <div class="sub_top fl">单价</div>
            <div class="sub_top fl">数量</div>
            <div class="sub_top fl">小计</div>
            <div class="sub_top fr">操作</div>
            <div class="clear"></div>
        </div>

        <c:forEach items="${cart.cartItems}" var="ci">

            <div class="content2 center">
                <div class="sub_content fl ">
                    <%--<input type="checkbox" value="quanxuan" class="quanxuan" />--%>
                </div>
                <div class="sub_content fl"><img src="<%=request.getContextPath()%>${ci.product.pimage}"></div>
                <div class="sub_content fl ft20">${ci.product.pname}</div>
                <div class="sub_content fl ">${ci.product.shop_price}</div>
                <div class="sub_content fl">
                    <input class="shuliang" readonly type="number" value="${ci.count}" step="1" min="1" >
                </div>
                <div class="sub_content fl">${ci.subtotal}</div>
                <div class="sub_content fl"><a href="javascript:void(0)" onclick="removeFromCart('${ci.product.pid}')">×</a></div>
                <div class="clear"></div>
            </div>

        </c:forEach>
        </c:if>
    </div>

    <div class="jiesuandan mt20 center">
        <div class="tishi fl ml20">
            <ul>
                <li><a href="<%=request.getContextPath()%>/CartServlet?method=clear">清空购物车</a></li>
                <%--<li>|</li>--%>
                <%--<li>共<span>2</span>件商品，已选择<span>1</span>件</li>--%>
                <div class="clear"></div>
            </ul>
        </div>
        <div class="jiesuan fr">
            <div class="jiesuanjiage fl">合计（不含运费）：<span>${cart.total}元</span></div>
            <div class="jsanniu fr"><input class="jsan" type="button" onclick="orderGenerate()" name="jiesuan"  value="去下单"/></div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>

</div>




<!-- footer -->
<footer class="center">

    <div class="mt20">小米商城|MIUI|米聊|多看书城|小米路由器|视频电话|小米天猫店|小米淘宝直营店|小米网盟|小米移动|隐私政策|Select Region</div>
    <div>©mi.com 京ICP证110507号 京ICP备10046444号 京公网安备11010802020134号 京网文[2014]0059-0009号</div>
    <div>违法和不良信息举报电话：185-0130-1238，本网站所列数据，除特殊说明，所有数据均出自我司实验室测试</div>
</footer>

</body>
<script type="text/javascript">
    function removeFromCart(pid) {
        if (confirm("你忍心抛弃我吗")) {
            var pathName=window.document.location.pathname;
            var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);
            location.href=projectName+"/CartServlet?method=remove&pid="+pid;
        }

    }

    function orderGenerate() {
        //pathName的值：例：/项目名/目录....
        var pathName=window.document.location.pathname;

        //截取项目的根目录
        var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);
        location.href= projectName+"/OrderServlet?method=save"

    }
</script>

</html>
