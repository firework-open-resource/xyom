<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: wangshibo
  Date: 2020/4/18
  Time: 11:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="author" content="order by dede58.com"/>
    <title>分类列表</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style.css">
    <script src="http://libs.baidu.com/jquery/2.1.4/jquery.min.js"></script>
</head>
<body>
<!-- start header -->
<%@include file="head.jsp"%>
<!--end header -->

<!-- start banner_x -->

<!-- end banner_x -->

<!-- start banner_y -->
<!-- end banner -->

<!-- start danpin -->
<div class="danpin center">

    <div class="biaoti center">小米手机</div>
    <div class="main center">
        <c:forEach items="${pb.data}" var="vr" begin="0" end="4">

        <div class="mingxing fl mb20" style="border:2px solid #fff;width:230px;cursor:pointer;" onmouseout="this.style.border='2px solid #fff'" onmousemove="this.style.border='2px solid red'">
            <div class="sub_mingxing"><a href="<%=request.getContextPath()%>/ProductServlet?method=getById&pid=${vr.pid}"><img src="<%=request.getContextPath()%>${vr.pimage}" alt=""></a></div>
            <div class="pinpai"><a href="<%=request.getContextPath()%>/ProductServlet?method=getById&pid=${vr.pid}" >${vr.pname}</a></div>
            <div class="youhui">${f:substring(vr.pdesc,0 ,10 )}...</div>
            <div class="jiage">￥${vr.shop_price}</div>
        </div>

        </c:forEach>


        <div class="clear"></div>
    </div>
    <div class="main center mb20" >

        <c:forEach items="${pb.data}" var="vr" begin="5" end="9">

            <div class="mingxing fl mb20" style="border:2px solid #fff;width:230px;cursor:pointer;" onmouseout="this.style.border='2px solid #fff'" onmousemove="this.style.border='2px solid red'">
                <div class="sub_mingxing"><a href="<%=request.getContextPath()%>/ProductServlet?method=getById&pid=${vr.pid}"><img src="<%=request.getContextPath()%>${vr.pimage}" alt=""></a></div>
                <div class="pinpai"><a href="<%=request.getContextPath()%>/ProductServlet?method=getById&pid=${vr.pid}">${vr.pname}</a></div>
                <div class="youhui">${f:substring(vr.pdesc,0 ,10 )}...}</div>
                <div class="jiage">￥${vr.shop_price}</div>
            </div>

        </c:forEach>

        <div class="clear"></div>
    </div>

    <%--分页--%>
    <div class="main center mb20" style="height: 100px;line-height: 30px;vertical-align: center">
        <p style="height: 30px;">&nbsp;</p>
        <p style="text-align: center">

        <c:if test="${pb.pageNumber>1}">
            <a href="<%=request.getContextPath()%>/ProductServlet?method=findByPage&pageNumber=1&cid=${param.cid}" >首页</a>
            <a href="<%=request.getContextPath()%>/ProductServlet?method=findByPage&pageNumber=${pb.pageNumber-1}&cid=${param.cid}">上一页</a>
        </c:if>
        <c:if test="${pb.pageNumber<pb.totalPage}">
            <a href="<%=request.getContextPath()%>/ProductServlet?method=findByPage&pageNumber=${pb.pageNumber+1}&cid=${param.cid}">下一页</a>
            <a href="<%=request.getContextPath()%>/ProductServlet?method=findByPage&pageNumber=${pb.totalPage}&cid=${param.cid}">末页</a>
        </c:if>

        </p>
        <form action="<%=request.getContextPath()%>/ProductServlet?method=findByPage&cid=${param.cid}" method="post">
            <h4 align="center" style="display: block">
                共${pb.totalPage}页
                <input type="number" style="border: 1px solid saddlebrown;" value="${pb.pageNumber}" min="1" max="${pb.totalPage}" name="pageNumber" size="1">页
                <input type="submit" value="到达">
            </h4>
        </form>

    </div>

</div>





<footer class="mt20 center">
    <div class="mt20">小米商城|MIUI|米聊|多看书城|小米路由器|视频电话|小米天猫店|小米淘宝直营店|小米网盟|小米移动|隐私政策|Select Region</div>
    <div>©mi.com 京ICP证110507号 京ICP备10046444号 京公网安备11010802020134号 京网文[2014]0059-0009号</div>
    <div>违法和不良信息举报电话：185-0130-1238，本网站所列数据，除特殊说明，所有数据均出自我司实验室测试</div>

</footer>

<!-- end danpin -->


</body>
</html>
